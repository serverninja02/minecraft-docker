Minecraft-Docker (with overviewer)
==================================

Requirements:
------------
 - Docker
 - Docker-Compose (1.9.x or greater)
 - Rancher-Compose (use version that works with your rancher-server)

Setup (using Docker Compose):
-----------------------------

 - Run the following:
```
docker-compose up --build -d
```

 - Find your docker host:
```
echo $DOCKER_HOST
```

 - Then connect to it via minecraft

 - To see a map (generated every 15 minutes):
http://<ip_of_docker_host>:8000

Setup (using Rancher Compose):
------------------------------
Use rancher-compose to build out a stack (will name project to minecraft-compose - directory name)
```
rancher-compose --env-file rancher-kids.env --project-name minecraft-kids up --pull
```

Use rancher-compose to build out an additional stack in new project (we'll call this minecraft-parents)
```
rancher-compose --env-file rancher-parents.env --project-name minecraft-parents up --pull
```
